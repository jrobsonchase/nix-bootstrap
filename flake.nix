rec {
  description = "Minimal nix image with a self-bootstrapping nix store";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    # nix.url = "github:NixOS/nix";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs' = import nixpkgs {
          inherit system;
        };

        # break infinite recursion
        system' = system;

        builders = (pkgs'.callPackage ./buildimage.nix { inherit (pkgs'.writers) writePython3; });

        buildBootstrapBase =
          { nixpkgsUrl ? inputs.nixpkgs.url
          , system ? system'
          , pkgs ? pkgs'
          , nixStatic ? pkgs'.pkgsStatic.nix
          , name ? "nix-bootstrap"
          , buildFn ? builders.buildImageWithNixDb
          ,
          }:
          let
            inherit (pkgs') lib dockerTools closureInfo;
            inherit (pkgs) pkgsStatic;
            inherit (pkgsStatic) busybox cacert rsync;

            entrypoint = pkgs'.writeTextFile {
              name = "entrypoint";
              destination = "/nix-bootstrap-entrypoint";
              text = (builtins.readFile ./entrypoint);
              executable = true;
            };
            waitDaemon = pkgs'.writeScriptBin "nix-wait-daemon" ''
              #!/usr/bin/env ash
              if [ "''${DEBUG_ENTRYPOINT}" == "1" ]; then
                set -x
              fi

              while [ ! -S /nix/var/nix/daemon-socket/socket ]; do
                sleep 0.5
              done
            '';
            startDaemon = pkgs'.writeScriptBin "nix-start-daemon" ''
              #!/usr/bin/env ash
              if [ "''${DEBUG_ENTRYPOINT}" == "1" ]; then
                set -x
              fi
              NIX_DAEMON_LOG=''${NIX_DAEMON_LOG:-/dev/null}
              nix-daemon 2>>''${NIX_DAEMON_LOG} >>''${NIX_DAEMON_LOG} &
            '';
          in
          (buildFn rec {
            inherit name;
            tag = "latest";
            copyToRoot = [ nixStatic cacert startDaemon waitDaemon entrypoint ];
            extraCommands = ''
              set -x
          
              [ -n "$(ls *)" ] && chmod +w *
              mkdir -p sbin bin usr etc/nix etc/profile.d tmp var/tmp root

              echo "${nixpkgsUrl}" >> .nixpkgs-url

              ln -rs ./bin usr/bin
              ln -rs ./sbin usr/sbin
              ln -rs ./share usr/share
              ln -rs ./lib usr/lib
              ln -rs ./var usr/var
              cp -a ${busybox}/bin/* ./bin/

              ${import ./shadowSetup.nix}

              echo 'experimental-features = nix-command flakes' >> etc/nix/nix.conf

              chmod a+rwt tmp var/tmp
            '';
            config = {
              Entrypoint = [ "/nix-bootstrap-entrypoint" ];
              Env = [
                "PATH=/root/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
                "HOME=/root"
                "USER=root"
              ];
            };
          });

        name = "registry.gitlab.com/jrobsonchase/nix-bootstrap";

        nix-bootstrap = pkgs'.lib.makeOverridable buildBootstrapBase {
          inherit name;
        };
        nix-bootstrap-builder = builders.buildLayeredImageWithNixDb {
          name = nix-bootstrap.imageName + "/builder";
          tag = "latest";
          fromImage = nix-bootstrap.override {
            buildFn = builders.buildImage;
          };
          copyToRoot = with pkgs'; [ git bashInteractive procps ];
          config = {
            Cmd = [ "/bin/bash" ];
            Entrypoint = [ "/nix-bootstrap-entrypoint" ];
          };
        };

        build-and-push = with pkgs';
          writeShellScriptBin "build-and-push" ''
            PATH=${skopeo}/bin:${gzip}/bin:''${PATH}
            set -x
            ROOT="$(git rev-parse --show-toplevel)"
            IMAGE=''${1:-nix-bootstrap}
            IMAGE_NAME=''${2:-${nix-bootstrap.imageName}}
            IMAGE_TAG=''${3:-${nix-bootstrap.imageTag}}
            if nix build ''${ROOT}#''${IMAGE}; then
              gzip --fast -c result | \
              skopeo --insecure-policy \
                copy docker-archive:/dev/stdin docker://''${IMAGE_NAME}:''${IMAGE_TAG}
              if [ "$CI_COMMIT_SHORT_SHA" != "" ]; then
                gzip --fast -c result | \
                skopeo --insecure-policy \
                  copy docker-archive:/dev/stdin docker://''${IMAGE_NAME}:''${CI_COMMIT_SHORT_SHA}
              fi
            else
              nix log ''${ROOT}#''${IMAGE}
              exit 1
            fi
          '';
        run-container = with pkgs';
          writeShellScriptBin "run-container" ''
            PATH=${docker}/bin:''${PATH}
            set -e
            flags=""
            cmd=""
            delim=0
            for a in $@; do
              if [ "$a" == "--" ]; then
                delim=1
              fi
              if [ "$delim" == 1 ]; then
                cmd="$cmd $a"
              else
                flags="$flags $a"
              fi
            done
            ROOT="$(git rev-parse --show-toplevel)"
            set -x
            nix build ''${ROOT}#nix-bootstrap
            docker load -i result && docker run -it --rm ''${flags} ${nix-bootstrap.imageName}:${nix-bootstrap.imageTag} ''${cmd}
          '';
      in
      rec {
        packages = {
          inherit run-container nix-bootstrap build-and-push nix-bootstrap-builder;
        };

        defaultPackage = nix-bootstrap;
        lib = {
          inherit buildBootstrapBase;
        };

        devShells = rec {
          ciShell = pkgs'.mkShell {
            buildInputs = with pkgs'; [ run-container build-and-push gzip skopeo cachix ];
          };
          devShell = ciShell.overrideAttrs (attrs: {
            buildInputs = attrs.buildInputs ++ [ ];
          });
        };
        devShell = devShells.devShell;
      })) // {
      legacyPackages = nixpkgs.legacyPackages;
    };
}
